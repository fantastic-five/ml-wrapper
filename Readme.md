# How to use the API

The endpoint is exposed at  
POST /api/v1/queries  
{  
"url": URL_OF_YOUR_PICTURE  
}  

Response:  
{  
    "status": "success" or "failure",  
    "message": ERROR_MESSAGE           <- Only if error  
    "result": "ambrosia", "trash" OR "pothole",  
    "confidence": CONFIDENCE SCORE OF THIS RESULT  
}  


# For training
Tunnel to the machine via SSH  
Port 5001 -> ambrosia  
Port 5002 -> trash  
Port 5003 -> Potholes  