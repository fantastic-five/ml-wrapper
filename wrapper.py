from flask import Flask, request
from flask_restful import Resource, Api
import requests

app = Flask(__name__)
api = Api(app)


class PlantNetQuery:
    PLANT_NET_URL = "https://identify.plantnet-project.org/api/project" \
                    "/weurope/identify"
    PLANT_NET_PARAMS_TEMPLATE = {
        'imgs': None,
        'tags': 'leaf',
        'lang': 'en',
        'json': 'true',
        'app_version': 'web-1.0.0'
    }

    AMBROSIA_NAMES = {
        'ambrosia artemisiifolia',
        'ambrosia trifida',
        'ambrosia psilostachya',
        'ambrosia tenuifolia'
    }

    def query(self, picture_url):
        """
        Performs the picture query against the PLANT_NET webserver
        :param picture_url: url of the picture to query
        :return: confidence score for this picture form 0 to 1
        """
        if not picture_url:
            return None
        print("Querying Plant.net with" + picture_url)
        params = PlantNetQuery.PLANT_NET_PARAMS_TEMPLATE
        params['imgs'] = picture_url
        r = requests.get(PlantNetQuery.PLANT_NET_URL, params, timeout=15)
        if r.status_code != 200:
            raise ValueError('Status code: ' + str(r.status_code) +
                             'Message: ' + r.text)
        scores = []
        try:
            json = r.json()
            if json['status'].lower() != 'ok':
                raise ValueError("Status not ok. " + str(json['error_results']))
            for result in json['results']:
                if result['binomial'].lower() in PlantNetQuery.AMBROSIA_NAMES:
                    scores.append(float(result['score']))
        except KeyError as e:
            raise e
        return self._calc_score(scores)

    def _calc_score(self, scores):
        """
        Calculate the final score out of a list of scores
        :param scores: list of scores to weight
        :return: single final score from 0 - 1  or -1 on error
        """
        # just return the maximum value for now
        if len(scores) == 0:
            print("No Plant images results")
            raise ValueError("No results from Plant images")
        return max(scores) / 100


class MachineBoxQuery:
    MACHINE_BOX_URL = "http://{}/tagbox/check"

    def __init__(self, url, tag):
        """
        Initialize this box adapter with the tag to search for
        :return:
        """
        self._tag = tag
        self._url = MachineBoxQuery.MACHINE_BOX_URL.format(url)

    def query(self, picture_url):
        """
        Performs the picture query against the machinebox server
        :param picture_url: url of the picture to query
        :return: confidence score for this picture
        """
        if not picture_url:
            return None
        print("Querying " + self._tag + " with " + picture_url)
        params = {'url': picture_url}
        r = requests.post(self._url, params,
                          auth=('hackathon', 'pass'), timeout=15)
        if r.status_code != 200:
            raise ValueError('Status code: ' + str(r.status_code) +
                             'Message: ' + r.text)
        scores = {}
        try:
            json = r.json()
            if not json['success']:
                raise ValueError("Success is not true")
            for tag in json['custom_tags']:
                if tag['tag'] == self._tag:
                    scores[self._tag] = tag["confidence"]
        except Exception as e:
            raise e
        return self._calc_score(scores)

    def _calc_score(self, scores):
        """
        Calculate the final confidence score from list of scores
        :param scores: dict of from tag to confidence score
        :return: final score
        """
        # Just return the confidence score for ambrosia now
        return scores.get(self._tag, 0)

pnQ = PlantNetQuery()

"""
mbQ_ambrosia = MachineBoxQuery("localhost:5001", "ambrosia")
mbQ_trash = MachineBoxQuery("localhost:5002", "trash")
mbQ_pothole = MachineBoxQuery("localhost:5003", "pothole")
"""
mbQ_ambrosia = MachineBoxQuery("ambrosia-box:8080", "ambrosia")
mbQ_trash = MachineBoxQuery("trash-box:8080", "trash")
mbQ_pothole = MachineBoxQuery("pothole-box:8080", "pothole")


class Queries(Resource):

    def post(self):
        """
        Accept and process request for new queries
        :return: json object with confidence rating or json object with error
        """
        json_data = request.get_json(force=True)
        if not json_data or json_data.get('url', '') == '':
            return {'status': 'failure', 'message': 'url not provided'}, 403
        try:
            machine_box_score = mbQ_ambrosia.query(json_data["url"])
            try:
                plant_net_score = pnQ.query(json_data["url"]) * 1.4
                # Do a simple average over both scores
                ambrosia_score = (plant_net_score + machine_box_score) / 2
                print("Plantnet: ", plant_net_score,
                      " MachineBox: ", machine_box_score)
            except Exception as e:
                print(e)
                ambrosia_score = machine_box_score
            trash_box_score = mbQ_trash.query(json_data["url"])
            pothole_box_score = mbQ_pothole.query(json_data["url"])
            print("ambrosia ", ambrosia_score)
            print("Trash ", trash_box_score)
            print("Pothole ", pothole_box_score)

            if trash_box_score > pothole_box_score and trash_box_score > \
                    ambrosia_score:
                result = "trash"
                score = trash_box_score
            elif pothole_box_score > ambrosia_score:
                result = "pothole"
                score = pothole_box_score
            else:
                result = "ambrosia"
                score = ambrosia_score

            return {'status': 'success', 'result': result, 'confidence': score}
        except ValueError as e:
            return {'status': 'failure', 'message': "Value Error " + str(e),
                    'confidence': 0}, 400
        except KeyError as e:
            return {'status': 'failure', 'message': "KeyError: " + str(e),
                    'confidence': 0}, 400

api.add_resource(Queries, '/api/v1/queries')


@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run(host='0.0.0.0')
