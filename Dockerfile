FROM python:3.6.5-alpine3.7
ADD wrapper.py requirements.txt /code/
WORKDIR /code
RUN pip install -r requirements.txt
ENTRYPOINT ["python", "-u", "wrapper.py"]